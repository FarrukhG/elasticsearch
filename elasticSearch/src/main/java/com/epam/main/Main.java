package com.epam.main;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.elasticsearch.hadoop.cfg.ConfigurationOptions;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("Connecting to Elasticsearch...");
            SparkSession spark = SparkSession.builder()
                    .config(ConfigurationOptions.ES_NODES, "localhost")
                    .config(ConfigurationOptions.ES_PORT, "9200")
                    .config(ConfigurationOptions.ES_NODES_WAN_ONLY, "true")
                    .appName("StreamingElastic")
                    .master("local[*]")
                    .getOrCreate();

            System.out.println("Reading CSV files...");
            Dataset<Row> df = spark.read()
                    .format("org.apache.spark.sql.execution.datasources.csv.CSVFileFormat")
                    .option("header", "true")
                    .load("src/main/resources/data/*.csv");

            String esIndex = "restaurants/data";

            System.out.println("Writing data to Elasticsearch...");
            df.write()
                    .format("org.elasticsearch.spark.sql")
                    .option("es.resource", esIndex)
                    .mode(SaveMode.Append)
                    .save();

            spark.stop();
            System.out.println("Done. Visit http://localhost:9200/restaurants");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}