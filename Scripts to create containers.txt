git clone https://github.com/docker/awesome-compose.git

cd awesome-compose/elasticsearch-logstash-kibana

mkdir -p logstash/pipeline

code logstash/pipeline/logstash.conf

cd ../../

code docker-compose.yml

docker-compose up -d